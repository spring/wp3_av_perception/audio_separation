import torch
from model.combined_loss import reorder_source_mse
name_model = "model_vad_recursive" #
if name_model=="vad_cosine":
    import model.model_vad_cosine as module_arch
elif name_model=="model_vad_recursive":   
    import model.model_updated as module_arch
elif name_model=="old_version":   
    import model.model_updated as module_arch
from parse_config import ConfigParser
import model.pit_wrapper as module_loss
from utils import read_json
import warnings
warnings.filterwarnings("ignore")
import numpy as np
from pathlib import Path
from scipy.io.wavfile import write

use_cuda = torch.cuda.is_available()
if use_cuda:
    device = torch.device("cuda")
    torch.set_default_tensor_type(torch.cuda.FloatTensor)
else: device = torch.device("cpu")


class SpeakerSeparator:
    def __init__(self, window_len, stride, model_path) -> None:
        """init a new instance of the SpeakerSeparator object

        Args:
            window_len (int): the size of the sliding window, recommended to be amultiple of stride
            stride (int): the differnce between each windw
            model (torch model): the model to use for inference
            criterion_similarity (optional): the similarity criterion to be used. Defaults to None.
        """
        self.__init_model(model_path)
        self.window_len = window_len
        self.stride = stride
        self.input_buffer = self.__normalize(torch.zeros((1,window_len), dtype=torch.float32))
        self.criterion_similarity = module_loss.PITLossWrapper(loss_func=torch.nn.L1Loss(), pit_from="pw_pt")
        self.output_cache = None
        self.num_buffer = 0
        # self.embeds_model = id_model()
        self.output_cache_isfull = False

    def __init_model(self,model_path):
        if name_model=="vad_cosine":
            config_name = "config_cosine" #config_vad_recursive, 
        elif name_model=="model_vad_recursive":   
            config_name = "config_vad_recursive" #config_vad_recursive, 
        config = ConfigParser(config=read_json(Path(__file__).parent/f'{config_name}.json'),
                            resume=model_path,trainer_or_tester="tester")
        self.model = config.init_obj('arch', module_arch)
        checkpoint = torch.load(model_path, map_location = device)
        state_dict = checkpoint['state_dict']
        save_test_path = config["tester"]["save_test_real"]
        self.model.to(device)
        self.model.load_state_dict(state_dict, strict=True)
        self.model.eval()
        
    
    def __normalize(self, audio):
        min, max = -2**15, 2**15
        return 1.8*(audio - min) / (max - min) - 0.9

    def __push_to_input_buffer(self, data):
        data = torch.reshape(data,(1,len(data)))
        self.input_buffer = torch.cat((self.input_buffer[:,self.stride:], data), -1)
    def check_vad(self,vad_vec):
            vad_sum = sum(vad_vec)
            if vad_sum>len(vad_vec)/2:
                return True
            else:
                return False
    def __push_to_output_cache(self,data):
        if self.output_cache is None:
            self.output_cache = torch.tensor(data, dtype=torch.float32)
            return
        
        if self.output_cache.shape[-1] < self.window_len - self.stride:
            cutoff = 0
        else:
            cutoff = data.shape[-1]
            self.output_cache_isfull = True
        
        self.output_cache = torch.cat((self.output_cache[:,:,cutoff:], data),-1)
        
    def __call__(self, delta):
        """takes audio of mixed speaker audio and returns seperated audio arrays

        Args:
            delta (tensor or ndarray): array of audio data, size should be equal to stride.
            audio data is expected to be encoded for 16-bit PCM (-2^15,2^15)

        Returns:
            tuple(array,array): two arrays of seperated speakers, dtype is int16
        """
        if len(delta) < self.stride:
            print('delta too short')
            delta.resize(self.stride)
        if len(delta) > self.stride:
            print('delta too long')
            delta.resize(self.stride)
        
        delta = torch.tensor(delta, dtype=torch.float32)
            
        delta = self.__normalize(delta)
        self.__push_to_input_buffer(delta)

        with torch.no_grad():
            pred_separation, output_vad, _ ,_ ,_, _ = self.model(self.input_buffer)

        if self.output_cache is not None and self.output_cache_isfull:
            pred_separation_similarity = pred_separation[:,:,:self.output_cache.shape[-1]]
            _, batch_indices_separation = self.criterion_similarity(pred_separation_similarity[:,:,:-self.stride], 
                                                                    self.output_cache[:,:,self.stride:],
                                                                    return_incides=True)
        else:
            batch_indices_separation = torch.tensor([[0,1]])
            
        pred_separation = reorder_source_mse(pred_separation, batch_indices_separation)
        separated_delta = pred_separation[:,:,-self.stride:]
        self.__push_to_output_cache(separated_delta)
        
        output1, output2 = separated_delta[:,0].cpu().detach().numpy()[0], separated_delta[:,1].cpu().detach().numpy()[0]
        output_vad_bin = torch.where(output_vad > 0.95, 1., 0.)
        output_vad_bin1, output_vad_bin2 = output_vad_bin[:,0].cpu().detach().numpy()[0], output_vad_bin[:,1].cpu().detach().numpy()[0]
        output_vad_bin1, output_vad_bin2 = self.check_vad(output_vad_bin1),self.check_vad(output_vad_bin2)
        output1, output2 = (output1*(2**15)).astype(np.int16), (output2*(2**15)).astype(np.int16)
        return (output1, output2), (output_vad_bin1, output_vad_bin2)