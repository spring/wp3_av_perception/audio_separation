#! /usr/bin/python3

from spring_msgs.msg import RawAudioData
import queue
import pyaudio
import rospy, rostopic
import numpy as np
from threading import Thread
from std_msgs.msg import String
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
import rosnode

from audio_gcc_doa.nodes.audio_gcc_doa_node import GCC

# NODE_NAME = rospy.get_name()
NODE_NAME = 'speaker_separation'
OUTPUT_TOPIC = rospy.get_param(f"/{NODE_NAME}/output_topic")
class AudioPublisher:
    def __init__(self,frame_size=512,rate=16000):
        self.samplerate=rate
        self.frame_size = frame_size
        self.diagnostics_frame = np.array([])

        self.rate = rospy.Rate(16000/512)
        self.pub_sp0 =rospy.Publisher(f'{OUTPUT_TOPIC}/speaker0',RawAudioData,queue_size=32)
        self.pub_sp1 =rospy.Publisher(f'{OUTPUT_TOPIC}/speaker1',RawAudioData,queue_size=32)
        self.ari_speech = None
        self.q=queue.Queue()
        self.gcc = GCC()
        self.hz = np.array([0,0])
        self.__up_to_speed()
        self.__is_alive()


    def __is_alive(self):
        pub = rospy.Publisher("~is_alive", String, queue_size=1)
        diagnostics_pub =rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=1)
        diagnostocs_arr = DiagnosticArray()
        def publish():
            r = rospy.Rate(1)
            while not rospy.is_shutdown():
                if len(np.unique(self.diagnostics_frame))>0:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.OK,name='Audio: Audio preprocessing: Separation',message='Speech separation Running OK')
                else:
                    msg1 = DiagnosticStatus(level=DiagnosticStatus.WARN,name='Audio: Audio preprocessing: Separation',message='Speech separation Not publishing well')

                if any(self.hz < 30):
                    msg2 = DiagnosticStatus(level=DiagnosticStatus.WARN,name='Audio: Audio preprocessing: Separation rate',message=f'Speech separation publishing slow. rate: {self.hz}')
                else:
                    msg2 = DiagnosticStatus(level=DiagnosticStatus.OK,name='Audio: Audio preprocessing: Separation rate',message=f'Speech separation publishing up to speed, rate: {self.hz}')


                pub.publish(str(rospy.Time.now().to_sec()))
                diagnostocs_arr.status = [msg1, msg2]
                diagnostocs_arr.header.stamp=rospy.Time.now()
                diagnostics_pub.publish(diagnostocs_arr)                
                r.sleep()

        t = Thread(target=publish)
        t.start()
        
    def __up_to_speed(self):
        def sub():
            topics = (f'{OUTPUT_TOPIC}/speaker0',f'{OUTPUT_TOPIC}/speaker1')
            h = rostopic.ROSTopicHz(-1)
            s0 = rospy.Subscriber(topics[0],rospy.AnyMsg,h.callback_hz,callback_args=topics[0])
            s1 = rospy.Subscriber(topics[1],rospy.AnyMsg,h.callback_hz,callback_args=topics[1])
            while not rospy.is_shutdown():
                rospy.sleep(1)
                speaker0_hz = h.get_hz(topics[0])
                speaker1_hz = h.get_hz(topics[1])
                if speaker0_hz and speaker1_hz:
                    self.hz[0]=speaker0_hz[0]
                    self.hz[1]=speaker1_hz[0]

        t = Thread(target=sub)
        t.start()
        
    def publish_frames(self):#,vad1,vad2,mix1,mix2,stamps):
        # if self.q.qsize():
        #     data = self.q.get()
        #     self.diagnostics_frame = data
        #     for i in range(32):
        #         msg1 = RawAudioData(nb_channel=1,rate=self.samplerate,            
        #             format=pyaudio.paInt16,sample_byte_size= 2, 
        #             nb_frames=self.frame_size,data=list((data[0][512*i:512*(i+1)])))
        #         msg1.header.stamp=stamps[i]
        #         msg2 = RawAudioData(nb_channel=1,rate=self.samplerate,            
        #             format=pyaudio.paInt16,sample_byte_size= 2, 
        #             nb_frames=self.frame_size,data=list((data[1][512*i:512*(i+1)])))
        #         msg2.header.stamp = stamps[i]
        #         msg1.header.frame_id = self.ari_speech
        #         msg2.header.frame_id = self.ari_speech
        #         self.pub_sp0.publish(msg1) 
        #         self.pub_sp1.publish(msg2)
        #         if (vad1 and not vad2):
        #             hot_ch =0
        #             self.gcc.get_angle(mix2,mix1,hot_ch)
        #         elif (vad2 and not vad1):
        #             hot_ch=1
        #             self.gcc.get_angle(mix2,mix1,hot_ch)
        #         self.rate.sleep()
        while True:
            data_pack = self.q.get(block=True, timeout=None) #blocks when q is empty
            data,mix1,mix2,vad1,vad2,ari_speech,stamps = \
                data_pack.separated_audio,\
                data_pack.mix1,\
                data_pack.mix2,\
                data_pack.vad1,\
                data_pack.vad2,\
                data_pack.ari_speech,\
                data_pack.stamps
                
            self.diagnostics_frame = data
            for i in range(32):
                msg1 = RawAudioData(nb_channel=1,rate=self.samplerate,            
                    format=pyaudio.paInt16,sample_byte_size= 2, 
                    nb_frames=self.frame_size,data=list((data[0][512*i:512*(i+1)])))
                msg1.header.stamp=stamps[i]
                msg2 = RawAudioData(nb_channel=1,rate=self.samplerate,            
                    format=pyaudio.paInt16,sample_byte_size= 2, 
                    nb_frames=self.frame_size,data=list((data[1][512*i:512*(i+1)])))
                msg2.header.stamp = stamps[i]
                msg1.header.frame_id = ari_speech
                msg2.header.frame_id = ari_speech
                self.pub_sp0.publish(msg1) 
                self.pub_sp1.publish(msg2)
                if (vad1 and not vad2):
                    hot_ch =0
                    self.gcc.get_angle(mix2,mix1,hot_ch)
                elif (vad2 and not vad1):
                    hot_ch=1
                    self.gcc.get_angle(mix2,mix1,hot_ch)
                self.rate.sleep()