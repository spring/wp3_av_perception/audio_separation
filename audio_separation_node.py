#! /usr/bin/python3
import numpy as np
import time
import rospy
from respeaker_ros.msg import RawAudioData
import torch
from input_buffer import InputBuffer
from speaker_seperation import SpeakerSeparator
from audio_publisher import AudioPublisher
from pathlib import Path
from speaker_seperation import name_model
from threading import Thread
from dataclasses import dataclass
from typing import Tuple, Literal

CHANNEL=1
WIN_LEN=49152
STRIDE=16384
NODE_NAME = 'speaker_separation'
INPUT_TOPIC = rospy.get_param(f"/{NODE_NAME}/input_topic")
FILTER_SIZE = rospy.get_param(f'/{NODE_NAME}/filter_size_param')

class EmptyBufferError(Exception):
    pass
@dataclass
class DataPack:
    original_audio : torch.Tensor
    separated_audio : Tuple[torch.Tensor,torch.Tensor]
    vad : Tuple[bool,bool]
    stamps : np.ndarray
    ari_speech :Literal['ARI',None]
    
    def mix1(self):
        return self.original_audio[0]
    def mix2(self):
        return self.original_audio[1]
    def vad1(self):
        return self.vad[0]
    def vad2(self):
        return self.vad[1]
    def data(self):
        return self.separated_audio

class AudioSeparation:
    def __init__(self):
        self.use_cuda = torch.cuda.is_available()
        if self.use_cuda:
            device = torch.device("cuda")
            torch.set_default_tensor_type(torch.cuda.FloatTensor)
        else: device = torch.device("cpu")
        self.device = device
        self.ari_speech_counter =0
        rospy.init_node(NODE_NAME)
        #load classes
        self.ari_speech = None
        if name_model=="vad_cosine":
            name_model_pth = "model_best_w_vad_cosine" #config_vad_recursive, 
        elif name_model=="model_vad_recursive":   
            name_model_pth = "model_best_w_vad" #config_vad_recursive, 
        self.separator = SpeakerSeparator(window_len = WIN_LEN,stride = STRIDE,model_path=Path(__file__).parent/f'{name_model_pth}.pth')
        self.buff = InputBuffer(size=STRIDE,dtype=torch.float32,device=device)
        self.gcc_buff =InputBuffer(size=STRIDE,dtype=torch.int16)
        self.stamps = np.zeros(int(STRIDE/512),dtype=rospy.rostime.Time)
        rospy.loginfo('Speaker Separation model loaded')
        #setup  subscriper
        self.p = AudioPublisher()
        rospy.Subscriber(INPUT_TOPIC,RawAudioData,self.callback,queue_size=64) #change queue size if needed
        t = Thread(target=self.p.publish_frames)
        t.start()

    def add_stamp(self,stamp):
        self.stamps = np.roll(self.stamps,-1)
        self.stamps[-1] = stamp

    def send_frames_to_publisher(self):#,audio,mix1,mix2,vad,ari_speech):
        audio = self.data_tuple[0]
        mix1 = self.data_tuple[1]
        mix2 = self.data_tuple[2]
        vad = self.data_tuple[3]
        ari_speech = self.data_tuple[4]
        self.p.ari_speech=ari_speech
        self.p.q.put(audio)
        self.p.publish_frames(vad[0],vad[1],mix1,mix2,self.stamps)
        
        
    def callback(self,msg):
        if INPUT_TOPIC == '/audio/raw_audio':
            ego_speech = np.array(msg.data).reshape(512,6)[:,5]
        else:
            ego_speech = np.array(msg.data)
        if len(np.unique(ego_speech)) > 1:
            self.ari_speech = 'ARI'
            self.ari_speech_counter=FILTER_SIZE
        else:
            self.ari_speech = None
            self.ari_speech_counter=max(0,self.ari_speech_counter-1)
        frame = np.array(msg.data).reshape(512,6)#[:,CHANNEL]
        frame_to_sep = torch.from_numpy(frame[:,CHANNEL]).to(device=self.device)
        
        if self.ari_speech or self.ari_speech_counter !=0:
            frame_to_sep *= 0

        gcc_frame = torch.from_numpy(frame[:,2])
        self.gcc_buff.push(gcc_frame)
        self.buff.push(frame_to_sep)
        self.add_stamp(msg.header.stamp)
        if self.buff.is_full():
            data = self.buff.try_read_buffer()
            gcc = self.gcc_buff.try_read_buffer()
            if data is not None:
                audio,vad = self.separator(data)
                self.p.q.put(DataPack(original_audio=data,\
                    separated_audio=audio,
                    vad=vad,
                    stamps=self.stamps,
                    ari_speech=self.ari_speech))
            else:
                raise EmptyBufferError(self)
            
   


if __name__=="__main__":
    s = AudioSeparation()
    rospy.spin()
