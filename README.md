# speaker-seperation

input: 
Raw audio (1 ch) from the reaspeaker_ros package
/audio/raw_audio


output:
Two raw audio streams:
/audio/separation/speaker0
/audio/separation/speaker1


Launch:

roslaunch audio_separation separation.launch
