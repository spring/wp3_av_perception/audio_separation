from scipy.io.wavfile import read, write
import numpy as np
from speaker_seperation import SpeakerSeparator
import argparse
import torch
from parse_config import ConfigParser
from pathlib import Path
from speaker_seperation import name_model
import glob
import re
from tqdm import tqdm
from model.combined_loss import reorder_source_mse

def check_vad(vad_vec):
    vad_sum = sum(vad_vec)
    if vad_sum>len(vad_vec)/2:
        return True
    else:
        return False

def main(path_model):    
    fs = 16000
    # path_audio = "/home/ros/ros4hri_ws/src/audio_separation/mixed.wav"
    # samplerate, audio = read(path_audio)
    # audio = np.array(audio, dtype=np.float32)
    # win_len = 49152
    # stride = 16384
    
    # audio = np.resize(audio,(len(audio)//stride+1)*stride)
    # audio = torch.from_numpy(audio)
    
    use_cuda = torch.cuda.is_available()
    if use_cuda:
        device = torch.device("cuda")
        torch.set_default_tensor_type(torch.cuda.FloatTensor)
    else: device = torch.device("cpu")
    

    min, max = -2**15, 2**15
    win_len = 49152
    stride = 16384
    sep = SpeakerSeparator(win_len, stride, path_model)
    path2data = "/non-enc-volume/datasets/LibriSpeech_mls_french_400_subset_1/audio"
    mode = "offline"
    save_path = f"/storage01/mordehay/analysis_separation/one_speaker_{mode}"
    Path(save_path).mkdir(parents=True, exist_ok=True)
    raw_recordings = glob.glob(path2data + "/*raw.wav")
    pattern = r'/(\d+_\d+_\d+)_raw\.wav$'
    
    for path_audio in tqdm(raw_recordings, total=len(raw_recordings)):
        match = re.search(pattern, path_audio)
        number = match.group(1)
        samplerate, audio = read(path_audio)
        audio = np.array(audio, dtype=np.float32)
        
        use_cuda = torch.cuda.is_available()
        if use_cuda:
            device = torch.device("cuda")
            torch.set_default_tensor_type(torch.cuda.FloatTensor)
        else: device = torch.device("cpu")
        
        if mode == "online":
            audio = np.resize(audio,(len(audio)//stride+1)*stride)
            audio = torch.from_numpy(audio)
            audio = audio.to(device)
            output1 = np.array([])
            output2 = np.array([])
            
            seg_audio = torch.reshape(audio,(len(audio)//stride, stride))
            
            for frame in seg_audio:
                # print(len(frame))
                output = sep(frame)
                output1 = np.concatenate((output1,output[0][0]))
                output2 = np.concatenate((output2,output[0][1]))
            write(Path(save_path).joinpath(f'{number}_out1.wav'), 16000, output1.astype(np.int16))
            write(Path(save_path).joinpath(f'{number}_out2.wav'), 16000, output2.astype(np.int16))
        elif mode == 'offline':
            
            audio =  1.8*(audio - min) / (max - min) - 0.9
            audio = np.array(audio, dtype=np.float32)
            audio = torch.from_numpy(audio)
            audio = audio.to(device)
            audio = torch.reshape(audio,(1, len(audio)))
            with torch.no_grad():
                pred_separation, output_vad, _ ,_ ,_, _ = sep.model(audio)


        
            output1, output2 = pred_separation[:,0].cpu().detach().numpy()[0], pred_separation[:,1].cpu().detach().numpy()[0]
            output_vad_bin = torch.where(output_vad > 0.95, 1., 0.)
            output_vad_bin1, output_vad_bin2 = output_vad_bin[:,0].cpu().detach().numpy()[0], output_vad_bin[:,1].cpu().detach().numpy()[0]
            output_vad_bin1, output_vad_bin2 = check_vad(output_vad_bin1), check_vad(output_vad_bin2)
            output1, output2 = (output1*(2**15)).astype(np.int16), (output2*(2**15)).astype(np.int16)

            write(Path(save_path).joinpath(f'{number}_out1.wav'), 16000, output1.astype(np.int16))
            write(Path(save_path).joinpath(f'{number}_out2.wav'), 16000, output2.astype(np.int16))
        # write('out1.wav', 16000, output1.astype(np.float32))
        # write('out2.wav', 16000, output2.astype(np.float32))


if __name__ == '__main__':
    if name_model=="vad_cosine":
        path_model = "/home/ros/ros4hri_ws/src/audio_separation/model_best_w_vad_cosine.pth" #config_vad_recursive, 
    elif name_model=="model_vad_recursive":   
        path_model = "/home/ros/ros4hri_ws/src/audio_separation/model_best_w_vad.pth" #config_vad_recursive, 
    main(path_model)