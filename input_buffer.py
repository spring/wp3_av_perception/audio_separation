import torch
import gc
class InputBuffer:
    def __init__(self,size, dtype, device=None) -> None:
        """ generic input buffer using torch,
        NOTES: trying to push  data beyond the buffer's defined size will raise a BufferException
        trying to read from the buffer when it is not full will return None

        Args:
            size (int): the size of the buffer, this is static
            dtype (dtype): the dtype of the tensor
            device (device, optional): the device on which the tensor will be used. Defaults to None.
        """
        self.data = torch.empty(size,dtype=dtype, device=device)
        self.size = size
        self.dtype = dtype
        self.device = device
        self.len = 0
        self.isfull = False
        
    def try_read_buffer(self):
        """try to read the buffer data, if the buffer is full, 
        all data will be returned, and the buffer will be reset
        otherwise return None

        Returns:
            tensor|None: the buffer's data
        """
        if self.isfull:
            data = torch.clone(self.data)
            self.data = torch.empty(self.size,
                                    dtype=self.dtype,
                                    device=self.device)
            self.len = 0
            self.isfull = False
            gc.collect()
            return data
        else:
            return None
        
    def __len__(self):
        return self.len
    
    def is_full(self):
        return self.isfull
    
    def push(self,data):
        """push data to the buffer

        Args:
            data (tensor): the data to be pushed

        Raises:
            BufferError: trying to push data over the size limit, empty the buffer before pushong more data
        """
        
        if len(self) + len(data) > self.size:
            raise BufferError(self)
        
        self.data[len(self):len(self)+len(data)] = torch.clone(data)
        self.len += len(data)
        
        if len(self) == self.size:
            self.isfull=True