from turtle import forward
import torch.nn as nn
import torch.nn.functional as F
# from STFT_Using_Conv1.STFT_Conv import STFT
# from base import BaseModel
import torch
import numpy as np
from torch.autograd import Variable
from torchaudio import transforms
from collections import OrderedDict

import torch.nn.utils.weight_norm as wn
import math

PI = torch.Tensor([math.pi])
class InverseMelSpec(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.inv_scale = transforms.InverseMelScale(n_mels=128, sample_rate=16000, n_stft=1028)
        self.inv_spec = transforms.InverseSpectrogram(n_fft=1028, hop_length=512, win_length=1028,
                                    window_fn=torch.hann_window)
    def forward(self, mel_spec, length):
        x = self.inv_scale(mel_spec)
        #print(x.shape)
        out = self.inv_spec(x, length=length)
        return out #out in time 
class InputSpec(nn.Module):
    def __init__(self, n_fft=512, hop_length=256, win_length=512) -> None:
        super().__init__()
        self.spec= transforms.Spectrogram(n_fft=n_fft, hop_length=hop_length, win_length=win_length,
                                      window_fn=torch.hann_window, power=None)  # for all channels
        
    def forward(self, sig_time):
        stft = self.spec(sig_time)
        stft[:, :, 0, :] = 0
        return stft 
    
class InputMelSpec(nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.spec= transforms.MelSpectrogram(sample_rate=16000, n_fft=512, win_length=512, hop_length=256, n_mels=128)
        
    def forward(self, sig_time):
        stft = self.spec(sig_time)
        stft[:, :, 0:3, :] = 0
        return stft     


class cLN(nn.Module):
    def __init__(self, dimension, eps=1e-8, trainable=True):
        super(cLN, self).__init__()

        self.eps = eps
        if trainable:
            self.gain = nn.Parameter(torch.ones(1, dimension, 1))
            self.bias = nn.Parameter(torch.zeros(1, dimension, 1))
        else:
            self.gain = Variable(torch.ones(1, dimension, 1), requires_grad=False)
            self.bias = Variable(torch.zeros(1, dimension, 1), requires_grad=False)

    def forward(self, input):
        # input size: (Batch, Freq, Time)
        # cumulative mean for each time step

        batch_size = input.size(0)
        channel = input.size(1)
        time_step = input.size(2)

        step_sum = input.sum(1)  # B, T
        step_pow_sum = input.pow(2).sum(1)  # B, T
        cum_sum = torch.cumsum(step_sum, dim=1)  # B, T
        cum_pow_sum = torch.cumsum(step_pow_sum, dim=1)  # B, T

        entry_cnt = np.arange(channel, channel * (time_step + 1), channel)
        entry_cnt = torch.from_numpy(entry_cnt).type(input.type())
        entry_cnt = entry_cnt.view(1, -1).expand_as(cum_sum)

        cum_mean = cum_sum / entry_cnt  # B, T
        cum_var = (cum_pow_sum - 2 * cum_mean * cum_sum) / entry_cnt + cum_mean.pow(2)  # B, T
        cum_std = (cum_var + self.eps).sqrt()  # B, T

        cum_mean = cum_mean.unsqueeze(1)
        cum_std = cum_std.unsqueeze(1)

        x = (input - cum_mean.expand_as(input)) / cum_std.expand_as(input)
        return x * self.gain.expand_as(x).type(x.type()) + self.bias.expand_as(x).type(x.type())


class DepthConv1d(nn.Module):

    def __init__(self, input_channel, hidden_channel, kernel, padding, dilation=1, skip=False, causal=False, bool_drop=True,
                 drop_value=0.1, wen=False, middle_layer=False):
        super(DepthConv1d, self).__init__()
        #hidden_channel = 512
        #input_channel = 256
        self.causal = causal
        self.skip = skip
        self.bool_drop = bool_drop
        self.middle_layer = middle_layer
        if not wen:
            self.conv1d = nn.Conv1d(input_channel, input_channel, 1)

            if self.causal:
                self.padding = (kernel - 1) * dilation
            else:
                self.padding = padding
            groups = int(hidden_channel/2)
            self.dconv1d = nn.Conv1d(input_channel, hidden_channel, kernel, dilation=dilation,
                                    groups=groups,
                                    padding=self.padding) #Depth-wise convolution
            self.res_out = nn.Conv1d(hidden_channel, input_channel, 1)
            self.nonlinearity1 = nn.PReLU()
            self.nonlinearity2 = nn.PReLU()
            self.drop1 = nn.Dropout2d(drop_value)
            self.drop2 = nn.Dropout2d(drop_value)
            if self.causal:
                self.reg1 = cLN(hidden_channel, eps=1e-08)
                self.reg2 = cLN(hidden_channel, eps=1e-08)
            else:
                self.reg1 = nn.GroupNorm(1, input_channel, eps=1e-08)
                self.reg2 = nn.GroupNorm(1, hidden_channel, eps=1e-08)

            if self.skip:
                self.skip_out = nn.Conv1d(hidden_channel, input_channel, 1)

        else:
            #print("Inside wen")
            self.conv1d = wn(nn.Conv1d(input_channel, input_channel, 1))

            if self.causal:
                self.padding = (kernel - 1) * dilation
            else:
                self.padding = padding
            groups = int(hidden_channel/2)
            self.dconv1d = wn(nn.Conv1d(input_channel, hidden_channel, kernel, dilation=dilation,
                                    groups=groups,
                                    padding=self.padding)) #Depth-wise convolution
            self.res_out = wn(nn.Conv1d(hidden_channel, input_channel, 1))
            self.nonlinearity1 = nn.PReLU()
            self.nonlinearity2 = nn.PReLU()
            self.drop1 = nn.Dropout2d(drop_value)
            self.drop2 = nn.Dropout2d(drop_value)
            if self.causal:
                self.reg1 = cLN(hidden_channel, eps=1e-08)
                self.reg2 = cLN(hidden_channel, eps=1e-08)
            else:
                self.reg1 = nn.GroupNorm(1, input_channel, eps=1e-08)
                self.reg2 = nn.GroupNorm(1, hidden_channel, eps=1e-08)

            if self.skip:
                self.skip_out = wn(nn.Conv1d(hidden_channel, input_channel, 1))
            if self.middle_layer:
                self.activity_middle = nn.Conv2d(in_channels=2, out_channels=2, kernel_size=(3,3), stride=1, padding=(1,1))
                self.prelu_middle = nn.PReLU()
                #self.gn_middle = nn.GroupNorm(1, hidden_channel, eps=1e-08)
    def forward(self, input):
        if self.bool_drop:
            output = self.reg1(self.drop1(self.nonlinearity1(self.conv1d(input))))#with dropout
        else:
            output = self.reg1(self.nonlinearity1(self.conv1d(input)))#without droput
        #output = self.reg1(self.drop1(self.nonlinearity1(self.conv1d(input))))#with dropout
        if self.causal:
            output = self.reg2(self.nonlinearity2(self.dconv1d(output)[:, :, :-self.padding]))
        else:
            #print(output.shape)
            if self.bool_drop:
                output = self.reg2(self.drop2(self.nonlinearity2(self.dconv1d(output))))
            else:
                output = self.reg2(self.nonlinearity2(self.dconv1d(output))) #without dropout
            #output = self.reg2(self.drop2(self.nonlinearity2(self.dconv1d(output)))) #with dropout
            #print(output.shape)
        residual = self.res_out(output)
        if self.skip:
            skip = self.skip_out(output)
            if self.middle_layer:
                B, F, T = output.shape
                output = torch.reshape(output, (B, 2, -1, T))
                #output = self.prelu_middle(torch.squeeze(self.activity_middle(torch.unsqueeze(output, dim=1)), dim=1) * output)
                #output = output - self.prelu_middle(torch.squeeze(self.activity_middle(torch.unsqueeze(output, dim=1)), dim=1))
                output = output - self.prelu_middle(self.activity_middle(output))
                return residual, skip, output
            return residual, skip
        else:
            if self.middle_layer:
                B, F, T = output.shape
                output = torch.reshape(output, (B, 2, -1, T))
                #output = self.prelu_middle(torch.squeeze(self.activity_middle(torch.unsqueeze(output, dim=1)), dim=1) * output)
                #output = output - self.prelu_middle(torch.squeeze(self.activity_middle(torch.unsqueeze(output, dim=1)), dim=1))
                output = output - self.prelu_middle(self.activity_middle(output))
                return residual, output
            return residual
            


class VAD(nn.Module):
    def __init__(self, f_mul_spk, num_spk, wen=True):
        super().__init__()
        self.num_spk = num_spk
        out_channel1 = int(f_mul_spk/num_spk)
        padding = 5//2
        if not wen:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', wn(nn.Conv1d(in_channels=f_mul_spk, out_channels=out_channel1, kernel_size=5, stride=1, padding=padding))),
            ('relu_1', nn.PReLU()),
            ('BN_1', nn.GroupNorm(1, out_channel1, eps=1e-8))
            ]))


            self.vad_branch = nn.Sequential(OrderedDict([
            ('conv1_vad_branch', wn(nn.Conv1d(in_channels=out_channel1, out_channels=32, kernel_size=3, stride=1, padding=1))),
            ('relu_1_', nn.PReLU()),
            ('BN_2', nn.GroupNorm(1, 32, eps=1e-8)),
            ('conv1_3', wn(nn.Conv1d(in_channels=32, out_channels=8, kernel_size=3, stride=1, padding=1))),
            ('relu_3', nn.PReLU())
            ]))    
            """self.vad_branch = nn.Sequential(OrderedDict([
            ('conv1_vad_branch', nn.Conv1d(in_channels=out_channel1, out_channels=128, kernel_size=3, stride=1, padding=1)),
            ('relu_1_', nn.ReLU()),
            ('BN_2', nn.GroupNorm(1, 128, eps=1e-8)),
            ('conv1_3', nn.Conv1d(in_channels=128, out_channels=32, kernel_size=3, stride=1, padding=1)),
            ('relu_3', nn.ReLU()),
            ('BN_3', nn.GroupNorm(1, 32, eps=1e-8)),
            ('conv1_4', nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)),
            ('relu_4', nn.ReLU()),
            ('BN_4',nn.GroupNorm(1, 16, eps=1e-8))
            ]))"""

            

            """self.block2 = nn.Sequential(OrderedDict([
            ('conv1_2', nn.Conv1d(in_channels=out_channel1, out_channels=128, kernel_size=3, stride=1, padding=1)),
            ('relu_2', nn.ReLU()),
            ('BN_2', nn.BatchNorm1d(128))
            ]))
            
            self.block3 = nn.Sequential(OrderedDict([
            ('conv1_3', nn.Conv1d(in_channels=128, out_channels=32, kernel_size=3, stride=1, padding=1)),
            ('relu_3', nn.ReLU()),
            ('BN_3', nn.BatchNorm1d(32))
            ]))

            self.block4 = nn.Sequential(OrderedDict([
            ('conv1_4', nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)),
            ('relu_4', nn.ReLU()),
            ('BN_4', nn.BatchNorm1d(16))
            ]))"""

            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = nn.ModuleList([wn(nn.Conv1d(in_channels=8, out_channels=1, kernel_size=3, stride=1, padding=1)) for i in range(num_spk)])
        else:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', nn.Conv1d(in_channels=f_mul_spk, out_channels=out_channel1, kernel_size=5, stride=1, padding=padding)),
            ('relu_1', nn.ReLU()),
            ('BN_1', nn.GroupNorm(1, out_channel1, eps=1e-8))
            ]))


            self.vad_branch = nn.Sequential(OrderedDict([
            ('conv1_vad_branch', nn.Conv1d(in_channels=out_channel1, out_channels=32, kernel_size=3, stride=1, padding=1)),
            ('relu_1_', nn.ReLU()),
            ('BN_2', nn.GroupNorm(1, 32, eps=1e-8)),
            ('conv1_3', nn.Conv1d(in_channels=32, out_channels=8, kernel_size=3, stride=1, padding=1)),
            ('relu_3', nn.ReLU())
            ]))    
            """self.vad_branch = nn.Sequential(OrderedDict([
            ('conv1_vad_branch', nn.Conv1d(in_channels=out_channel1, out_channels=128, kernel_size=3, stride=1, padding=1)),
            ('relu_1_', nn.ReLU()),
            ('BN_2', nn.GroupNorm(1, 128, eps=1e-8)),
            ('conv1_3', nn.Conv1d(in_channels=128, out_channels=32, kernel_size=3, stride=1, padding=1)),
            ('relu_3', nn.ReLU()),
            ('BN_3', nn.GroupNorm(1, 32, eps=1e-8)),
            ('conv1_4', nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)),
            ('relu_4', nn.ReLU()),
            ('BN_4',nn.GroupNorm(1, 16, eps=1e-8))
            ]))"""

            

            """self.block2 = nn.Sequential(OrderedDict([
            ('conv1_2', nn.Conv1d(in_channels=out_channel1, out_channels=128, kernel_size=3, stride=1, padding=1)),
            ('relu_2', nn.ReLU()),
            ('BN_2', nn.BatchNorm1d(128))
            ]))
            
            self.block3 = nn.Sequential(OrderedDict([
            ('conv1_3', nn.Conv1d(in_channels=128, out_channels=32, kernel_size=3, stride=1, padding=1)),
            ('relu_3', nn.ReLU()),
            ('BN_3', nn.BatchNorm1d(32))
            ]))

            self.block4 = nn.Sequential(OrderedDict([
            ('conv1_4', nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)),
            ('relu_4', nn.ReLU()),
            ('BN_4', nn.BatchNorm1d(16))
            ]))"""

            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=8, out_channels=1, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
        #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
        self.sigmoid = nn.Sigmoid()

    def forward(self, input):
        #input shape is [B, F*n_spk, num_frames]
        out_common = self.common(input)
        out_vad = self.vad_branch(out_common)
        output_vad = [self.output_layer_vad[i](out_vad) for i in range(self.num_spk)] #list of object with shape [B, 1, num_frames]
        output_vad = torch.cat(output_vad, dim=1) #shape = [B, num_spk * 1,  num_frames]
        #print(output_vad.shape)
        output_vad = self.sigmoid(output_vad)

        return output_vad
    
    
    
    
class SMALL_VAD(nn.Module):
    def __init__(self, f_mul_spk=256, num_spk=32, wen=True):
        super().__init__()
        self.num_spk = num_spk
        out_channel1 = int(f_mul_spk/num_spk)
        padding = 5//2
        if wen:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', wn(nn.Conv1d(in_channels=256, out_channels=4, kernel_size=5, stride=1, padding=padding))),
            ('relu_1', nn.PReLU()),
            ('BN_1', nn.GroupNorm(1, 4, eps=1e-8))
            ]))


            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = wn(nn.Conv1d(in_channels=4, out_channels=1, kernel_size=3, stride=1, padding=1))
        else:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', nn.Conv1d(in_channels=256, out_channels=4, kernel_size=5, stride=1, padding=padding)),
            ('relu_1', nn.PReLU()),
            ('BN_1', nn.GroupNorm(1, 4, eps=1e-8))
            ]))

            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = nn.Conv1d(in_channels=4, out_channels=1, kernel_size=3, stride=1, padding=1)
        #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
        self.sigmoid = nn.Sigmoid()

    def forward(self, input):
        #input shape is [B, F*n_spk, num_frames]
        out_common = self.common(input)
        output_vad = self.output_layer_vad(out_common)#list of object with shape [B, 1, num_frames]
        output_vad = self.sigmoid(output_vad)

        return output_vad

class SMALL_VAD2(nn.Module):
    def __init__(self, f_mul_spk=257, num_spk=2, wen=True):
        super().__init__()
        self.num_spk = num_spk
        padding = 5//2
        if wen:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', wn(nn.Conv1d(in_channels=257, out_channels=4, kernel_size=5, stride=1, padding=padding))),
            ('relu_1', nn.PReLU()),
            ('BN_1', nn.GroupNorm(1, 4, eps=1e-8))
            ]))


            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = wn(nn.Conv1d(in_channels=4, out_channels=1, kernel_size=3, stride=1, padding=1))
        else:
            self.common = nn.Sequential(OrderedDict([
            ('conv1_1', nn.Conv1d(in_channels=257, out_channels=4, kernel_size=5, stride=1, padding=padding)),
            ('relu_1', nn.PReLU()),
            ('BN_1', nn.GroupNorm(1, 4, eps=1e-8))
            ]))

            #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
            self.output_layer_vad = nn.Conv1d(in_channels=4, out_channels=1, kernel_size=3, stride=1, padding=1)
        #self.output_layer_vad = nn.ModuleList([nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, stride=1, padding=1) for i in range(num_spk)])
        self.sigmoid = nn.Sigmoid()

    def forward(self, input):
        #input shape is [B, F*n_spk, num_frames]
        out_common = self.common(input)
        output_vad = self.output_layer_vad(out_common)#list of object with shape [B, 1, num_frames]
        output_vad = self.sigmoid(output_vad)

        return output_vad


class TCN(nn.Module): #this is the audio blocks
    def __init__(self, input_dim, output_dim, BN_dim, H_dim,
                 layer, R, kernel=3, skip=False,
                 causal=False, dilated=True, bool_drop=True, drop_value=0.1, wen=False, middle_vad=False, complex_masks=False, middle_layer=False):
        super(TCN, self).__init__()
        self.layer = layer
        # input is a sequence of features of shape (B, N, L)
        #input_dim = 257
        # BN_dim = 256
        self.middle_vad = middle_vad
        self.middle_layer = middle_layer
        if middle_vad:
            self.vad = SMALL_VAD() #VAD(BN_dim, 2, wen=wen)
        # normalization
        if not wen:
            if not causal:
                self.LN = nn.GroupNorm(1, input_dim, eps=1e-8) #this is like layer normalization because the number of groups is equal to one
            else:
                self.LN = cLN(input_dim, eps=1e-8)

            self.BN = nn.Conv1d(input_dim, BN_dim, 1)

            # TCN for feature extraction
            self.receptive_field = 0
            self.dilated = dilated

            self.TCN = nn.ModuleList([])
            for r in range(R):
                for i in range(layer):
                    if self.dilated:
                        if i == 0:
                            self.TCN.append(DepthConv1d(BN_dim, H_dim, kernel, dilation=1, padding=1, skip=skip,
                                                        causal=causal, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_layer=middle_layer)) 
                        else:
                            self.TCN.append(DepthConv1d(BN_dim, H_dim, kernel, dilation=2 * i, padding=2 * i, skip=skip,
                                                    causal=causal, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_layer=middle_layer)) ##I vhange to multiply and not square
                    else:
                        self.TCN.append(
                            DepthConv1d(BN_dim, H_dim, kernel, dilation=1, padding=1, skip=skip, causal=causal, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_layer=middle_layer))
                    if i == 0 and r == 0:
                        self.receptive_field += kernel
                    else:
                        if self.dilated:
                            self.receptive_field += (kernel - 1) * 2 ** i
                        else:
                            self.receptive_field += (kernel - 1)

            # print("Receptive field: {:3d} frames.".format(self.receptive_field))

            # output layer
            if complex_masks:
                self.output = nn.Sequential(nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim),
                                        nn.Conv1d(BN_dim, BN_dim*2, 1),
                                        nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim*2),
                                        nn.Conv1d(BN_dim*2, BN_dim*3, 1),
                                        nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim*3),
                                        nn.Conv1d(BN_dim*3, output_dim, 1)
                                        )
            else:
                self.output = nn.Sequential(nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim),
                                        nn.Conv1d(BN_dim, output_dim, 1)
                                        )

            self.skip = skip

        else:
            if not causal:
                self.LN = nn.GroupNorm(1, input_dim, eps=1e-8) #this is like layer normalization because the number of groups is equal to one
            else:
                self.LN = cLN(input_dim, eps=1e-8)

            self.BN = wn(nn.Conv1d(input_dim, BN_dim, 1))

            # TCN for feature extraction
            self.receptive_field = 0
            self.dilated = dilated

            self.TCN = nn.ModuleList([])
            for r in range(R):
                for i in range(layer):
                    if self.dilated:
                        if i == 0:
                            self.TCN.append(DepthConv1d(BN_dim, H_dim, kernel, dilation=1, padding=1, skip=skip,
                                                        causal=causal, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_layer=middle_layer)) 
                        else:
                            self.TCN.append(DepthConv1d(BN_dim, H_dim, kernel, dilation=i%4+1, padding=i%4+1, skip=skip,
                                                    causal=causal, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_layer=middle_layer)) ##I vhange to multiply and not square
                    else: # 2 * i
                        self.TCN.append(
                            DepthConv1d(BN_dim, H_dim, kernel, dilation=1, padding=1, skip=skip, causal=causal, bool_drop=bool_drop,
                                        drop_value=drop_value, wen=wen, middle_layer=middle_layer))
                    if i == 0 and r == 0:
                        self.receptive_field += kernel
                    else:
                        if self.dilated:
                            self.receptive_field += (kernel - 1) * 2 ** i
                        else:
                            self.receptive_field += (kernel - 1)

            # print("Receptive field: {:3d} frames.".format(self.receptive_field))

            # output layer
            if complex_masks:
                self.output = nn.Sequential(nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim),
                                        wn(nn.Conv1d(BN_dim, BN_dim*2, 1)),
                                        nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim*2),
                                        wn(nn.Conv1d(BN_dim*2, BN_dim*3, 1)),
                                        nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim*3),
                                        wn(nn.Conv1d(BN_dim*3, output_dim, 1))
                                        )
            else:
                """self.dconv_out = nn.Sequential(nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim),
                                        wn(nn.Conv1d(BN_dim, output_dim, 1))
                                        )"""
                self.output = nn.Sequential(nn.PReLU(),
                                        nn.GroupNorm(1, BN_dim),
                                        wn(nn.Conv1d(BN_dim, output_dim, 1))
                                        )
            

            self.skip = skip




    def forward(self, input, vas_sum_label=0):

        # input shape: (B, N, L)

        # normalization
        #print(type(input))
        #print(input.shape)
        out_mean_p_list = []
        x = self.LN(input)
        output = self.BN(x)

        # pass to TCN
        if self.skip:
            skip_connection = 0.
            for i in range(len(self.TCN)):
                residual, skip = self.TCN[i](output)
                output = output + residual
                skip_connection = skip_connection + skip
        else:
            #print(len(self.TCN))
            for i in range(len(self.TCN)):
                if self.middle_vad and i==(1 * self.layer): #In the start of R=1
                    output_middle_vad = self.vad(output)
                    #weights_vad = torch.mean(output_middle_vad, dim=1, keepdim=True)
                    #output = output * (1 + output_middle_vad)
                    #output_middle_vad_integer = torch.heaviside(output_middle_vad - 0.5, values=torch.tensor([0.], device=output_middle_vad.device))
                    output = torch.unsqueeze(vas_sum_label, dim=1) * output
                if self.middle_layer:
                    residual, out_mean_p = self.TCN[i](output)
                    if i % (1 * self.layer) == 0 and i != 0:
                        out_mean_p_list.append(out_mean_p) #each element in this list is with shape [B, 512, T]
                else:
                    residual = self.TCN[i](output)   
                output = output + residual
                
                    
                    


        # output layer
        if self.skip:
            output = self.output(skip_connection)
        else:
            output = self.output(output)

        if self.middle_vad:
            if self.middle_layer:
                return output, output_middle_vad, out_mean_p_list
            return output, output_middle_vad #asumming there is no skipping
        else:
            if self.middle_layer:
                return output, out_mean_p_list
            return output


class CSD(nn.Module):
    def __init__(self, n_fftBins_h):
        super().__init__()
        
        self.n_fftBins_h = n_fftBins_h
        #self.spec = transforms.Spectrogram(n_fft=self.n_fftBins, hop_length=256, win_length=self.n_fftBins,
        #                              window_fn=torch.hann_window, power=None)  # for all channels
        self.am_to_db = transforms.AmplitudeToDB(stype="power")
        
        padding = 5//2
        self.later_norm = nn.GroupNorm(1, self.n_fftBins_h, eps=1e-8)
        self.block1 = nn.Sequential(OrderedDict([
          ('conv1_1', nn.Conv1d(in_channels=self.n_fftBins_h, out_channels=self.n_fftBins_h, kernel_size=5, stride=1,
                                dilation=2 ** 0, padding=2 ** 0 + (5//2 - 1))),
          ('prelu_1', nn.PReLU()),
          ('LN_1', nn.GroupNorm(1, self.n_fftBins_h, eps=1e-8))
        ]))

        self.block2 = nn.Sequential(OrderedDict([
          ('conv1_2', nn.Conv1d(in_channels=self.n_fftBins_h, out_channels=64, kernel_size=3, stride=1,
                                dilation=2 ** 1, padding=2 ** 1 + (3//2 - 1))),
          ('prelu_2', nn.PReLU()),
          ('LN_2', nn.GroupNorm(1, 64, eps=1e-8))
        ]))
        
        self.block3 = nn.Sequential(OrderedDict([
          ('conv1_3', nn.Conv1d(in_channels=64, out_channels=16, kernel_size=3, stride=1, dilation=2 ** 2,
                                padding=2 ** 2 + (3//2 - 1))),
          ('prelu_3', nn.PReLU()),
          ('LN_3', nn.GroupNorm(1, 16, eps=1e-8))
        ]))

        self.block4 = nn.Sequential(OrderedDict([
          ('conv1_4', nn.Conv1d(in_channels=16, out_channels=8, kernel_size=3, stride=1, dilation=2 ** 3, padding=2 ** 3 + (3//2 - 1))),
          ('prelu_4', nn.PReLU()),
          ('LN_4', nn.GroupNorm(1, 8, eps=1e-8))
        ]))

        self.output_layer = nn.Sequential(OrderedDict([
          ('out_conv1', nn.Conv1d(in_channels=8, out_channels=3, kernel_size=3, dilation=2 ** 0, padding=2 ** 0 + (3//2 - 1))),
          ('out_prelu_4', nn.PReLU())
        ]))
        


    def forward(self, input):
        #input shape is [B, samples in time]
        #stft = self.spec(input)
        #stft[:, 0, :] = 0
        #print(f"the stft shape is: {stft.shape}")
        
        power = torch.pow(torch.abs(input), 2) #shape=[B, F, T]
        power = self.later_norm(power)
        #maybe we need to swap these two layers
        spectrum = self.am_to_db(power)
        out = self.block1(spectrum)
        out = self.block2(out)
        out = self.block3(out)
        out = self.block4(out)
        spp = self.output_layer(out)
        return spp


def csd_model(n_fftBins_h, pretrained:bool=True, path_pretrained_model:str="", freeze_all:bool=True, finetune:bool=True) -> CSD:
    if pretrained:
        path_csd_model = str(path_pretrained_model)
        checkpoint = torch.load(path_csd_model)
        model = CSD(n_fftBins_h)
        model.load_state_dict(checkpoint['state_dict'])
        if freeze_all:
            for param in model.parameters():
                param.requires_grad = False
            """for (name, module) in model.named_children():
                for layer in module.children():
                    print(layer)
                    for param in layer.parameters():
                        param.requires_grad = False"""
            if finetune:
                print("finetune")
                for param in model.output_layer.parameters():
                    param.requires_grad = True
            else:
                 return model
        return model
    return CSD(n_fftBins_h)


class SeparationModel(nn.Module):
    def __init__(self, n_fftBins=512, BN_dim=256, H_dim=512, layer=8, stack=3, kernel=3,
     num_spk=2, skip=False, dilated=True, casual=False, pretrained_csd=True, path_pretrained_csd=None,
     finetune=True, freeze_csd_weights=True, bool_drop=True, drop_value=0.1, wen=False, mel_bool=False, middle_vad=False, final_vad=True, csd_bool=False,
     noisy_phase=False, complex_masks=False, activity_input_bool=False, pc_bool=False, middle_layer=False):
        super(SeparationModel, self).__init__()
        self.n_fftBins = n_fftBins
        self.n_fftBins_h = n_fftBins//2 + 1
        self.df_freq = (n_fftBins//2 + 1)*(num_spk*2+1) #ToDO df
        self.BN_dim = BN_dim
        self.layer = layer
        self.stack = stack
        self.kernel = kernel
        self.num_spk = num_spk
        self.H_dim = H_dim
        self.skip = skip
        self.dilated = dilated
        self.casual = casual
        self.middle_vad = middle_vad
        self.noisy_phase = noisy_phase
        self.final_vad = final_vad
        self.csd_bool = csd_bool
        self.complex_masks = complex_masks
        self.activity_input_bool = activity_input_bool
        self.pc_bool = pc_bool
        self.middle_layer = middle_layer

        print("begin of model")
        # self.STFT = STFT(filter_length=512, hop_length=256, win_length=None, window='hann')
        self.am_to_db = transforms.AmplitudeToDB(stype="power")
        if mel_bool:
            self.spec_input = InputMelSpec()
            input_tcn = 128
            if self.middle_layer:
                output_tcn = (self.n_fftBins_h - 1)*self.num_spk
            else:
                output_tcn = self.n_fftBins_h * self.num_spk
            if self.complex_masks:
                input_tcn *= 2
                output_tcn *= 2
            self.TCN = TCN(input_tcn, output_tcn, self.BN_dim, self.H_dim,
                        self.layer, self.stack, kernel=3, skip=self.skip,
                        causal=self.casual, dilated=self.dilated, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_vad=self.middle_vad,
                        complex_masks=self.complex_masks, middle_layer=middle_layer) #ToDO df    
            #self.inv_spec = InverseMelSpec()
            
        else:
            self.spec_input = InputSpec(n_fft=self.n_fftBins, hop_length=int(self.n_fftBins/2), win_length=self.n_fftBins)
            input_tcn = self.n_fftBins_h
            if self.middle_layer:
                output_tcn = (self.n_fftBins_h - 1)*self.num_spk
            else:
                output_tcn = self.n_fftBins_h * self.num_spk
            if self.complex_masks:
                input_tcn *= 2
                output_tcn *= 2
            self.TCN = TCN(input_tcn, output_tcn, self.BN_dim, self.H_dim,
                        self.layer, self.stack, kernel=3, skip=self.skip,
                        causal=self.casual, dilated=self.dilated, bool_drop=bool_drop, drop_value=drop_value, wen=wen, middle_vad=self.middle_vad,
                        complex_masks=self.complex_masks, middle_layer=middle_layer) #ToDO df    
        
              
        self.spec_output = transforms.Spectrogram(n_fft=self.n_fftBins, hop_length=int(self.n_fftBins/2), win_length=self.n_fftBins,
                                      window_fn=torch.hann_window, power=None)  # for all channels                
        self.to_spec = nn.Conv1d(self.BN_dim, self.num_spk*self.n_fftBins_h, 1)
        if self.csd_bool:
            self.csd_model = csd_model(self.n_fftBins_h, pretrained=pretrained_csd, path_pretrained_model=path_pretrained_csd,
                                   freeze_all=freeze_csd_weights, finetune=finetune)
        #self.df_layer = DirectionalFeature()
        if self.final_vad:
            self.vad = SMALL_VAD2() #VAD(self.n_fftBins_h*self.num_spk, self.num_spk, wen=wen)   ###need to change to old vad
        self.inv_spec = transforms.InverseSpectrogram(n_fft=self.n_fftBins, hop_length=int(self.n_fftBins/2), win_length=self.n_fftBins,
                                    window_fn=torch.hann_window)
        self.m = nn.Sigmoid()
        #self.slope = nn.Parameter(torch.ones(1))
        self.softmax = torch.nn.Softmax(dim=1)
        
        ####
        #mus = torch.ones((input_tcn, out_tcn))
        #nn.parameter.Parameter(data=)
        if self.activity_input_bool:
            self.activity_input = nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))
            self.prelu = nn.PReLU()
        #self.m2 = nn.Sigmoid()
        """self.activity_input = nn.Sequential(OrderedDict([
            ('conv2d_1', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('prelu_1', nn.PReLU()),
            ('GN_1', nn.GroupNorm(1, 1, eps=1e-8)),
            ('conv2d_2', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('prelu_2', nn.PReLU()),
            ('GN_2', nn.GroupNorm(1, 1, eps=1e-8)),
            ('conv2d_3', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('GN_3', nn.GroupNorm(1, 1, eps=1e-8)),
            ('sigmoid', nn.Sigmoid())
            ])) """
        
        """self.activity_output = nn.Sequential(OrderedDict([
            ('conv2d_1', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('prelu_1', nn.PReLU()),
            ('GN_1', nn.GroupNorm(1, 1, eps=1e-8)),
            ('conv2d_2', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('prelu_2', nn.PReLU()),
            ('GN_2', nn.GroupNorm(1, 1, eps=1e-8)),
            ('conv2d_3', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
            ('GN_3', nn.GroupNorm(1, 1, eps=1e-8)),
            ('sigmoid', nn.Sigmoid())
            ])) """
        """self.activity_output = nn.Sequential(OrderedDict([
        ('conv2d_1', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
        ('prelu_1', nn.PReLU()),
        ('GN_1', nn.GroupNorm(1, 1, eps=1e-8)),
        ('conv2d_2', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
        ('prelu_2', nn.PReLU()),
        ('GN_2', nn.GroupNorm(1, 1, eps=1e-8)),
        ('conv2d_3', nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))),
        ('prelu_3', nn.PReLU()),
        ])) """
    # 512,257
        """self.decoder = nn.ConvTranspose1d(in_channels=256 * 2,
            out_channels=2,
            output_padding=(8 // 2) - 1,
            kernel_size=8,
            stride=8 // 2,
            padding=enc_kernel_size // 2,
            groups=1, bias=False)"""
        """self.correct_phase_layer = nn.Sequential(OrderedDict([
            ('conv2d_phase', wn(nn.Conv2d(in_channels=2, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1)))),
            ('GN_phase', nn.GroupNorm(1, 1, eps=1e-8))
            ]))"""
        if self.pc_bool:
            self.correct_phase_layer = nn.Sequential(OrderedDict([
                ('conv2d_phase1', wn(nn.Conv2d(in_channels=2, out_channels=2, kernel_size=(3,3), stride=1, padding=(1,1)))),
                ('prelu_phase1', nn.PReLU()),
                ('GN_phase1', nn.GroupNorm(1, 2, eps=1e-8)),
                ('conv2d_phase2', wn(nn.Conv2d(in_channels=2, out_channels=2, kernel_size=(3,3), stride=1, padding=(1,1)))),
                ('prelu_phase1', nn.PReLU()),
                ('GN_phase2', nn.GroupNorm(1, 2, eps=1e-8)),
                ('conv2d_phase3', wn(nn.Conv2d(in_channels=2, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1)))),
                ('prelu_phase1', nn.PReLU()),
                ('GN_phase3', nn.GroupNorm(1, 1, eps=1e-8))
                ]))
            self.abs_layer = wn(nn.Conv2d(in_channels=1, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1)))    
            self.tanh_phase = nn.Tanh()
            self.PI = math.pi
        """self.mask_conv2d = nn.Conv2d(1, 1, kernel_size=(1,3), stride=1, padding=(0,1))
        self.prelu_mask = nn.PReLU()"""
        if self.middle_layer:
            self.conv2d_middle = nn.Conv2d(in_channels=3, out_channels=1, kernel_size=(3,3), stride=1, padding=(1,1))
            #self.prelu_middle = nn.PReLU()
            #self.gn_middle = nn.GroupNorm(1, output_tcn, eps=1e-8)
            
    def forward(self, x, doa=0, vas_sum_label=0): #ToDO df
        #print(x.shape)
        # the input is samples of signals
        # df = df.float()
        x = x.unsqueeze(1)  # if x is single channel. Delete if x is multi-channel
        # print(x.shape)
        num_of_samples = x.shape[-1]
        stft_out = self.spec_output(x)
        stft = self.spec_input(x)
        #print(stft.shape)
        #print(stft.shape)
        stft_out[:, :, 0, :] = 0
        #print(stft_out.dtype)
        # print(f"the stft shape is: {stft.shape}")
        if self.complex_masks:
            
            ##########one option is inputting real and imaginary 
            real = stft.real
            imag = stft.imag
            self.spectrum = torch.cat((real[:, 0], imag[:, 0]), dim=1) #[B, 2F, T]
            #########another option is angle and abs value
            #print(stft.shape)
            #print(stft.abs().shape)
            """abs = stft.abs()
            ang = stft.angle()
            power = torch.pow(abs, 2)  # for multi channel, take only the channel zero, shape=[B, F, T]
            power_db = self.am_to_db(power)
            self.spectrum = torch.cat((power_db[:, 0], ang[:, 0]), dim=1) #[B, 2F, T]"""
           # print(self.spectrum.shape)
            #print(real.shape)
            
            ############if doing compress(like looking to listen)
            """mask = torch.zeros((self.spectrum.shape), device=self.spectrum.device)
            mask[self.spectrum > 0] = 1
            mask[self.spectrum < 0] = -1
            data = torch.pow(torch.abs(self.spectrum),0.3)
            self.spectrum = data * mask """     
            #################
            
            ############
        else:
            power = torch.pow(torch.abs(stft[:, 0, :, :]), 2)  # for multi channel, take only the channel zero, shape=[B, F, T]
            self.spectrum = self.am_to_db(power)
            ####Adding  activity in input
            if self.activity_input_bool:
                spec_activity = torch.squeeze(self.activity_input(torch.unsqueeze(self.spectrum, dim=1)), dim=1)
                spec_activity = self.prelu(spec_activity)
                self.spectrum = self.spectrum * spec_activity

        # with pitch
        # #############
        # pitch = stft_pitch_detect(torch.transpose(stft[:, 0, :, :], dim0=-2, dim1=-1))
        # #choose channel zero for pitch detect
        # pitch.transpose(-2, -1)
        # spectrum = torch.cat((pitch, spectrum), dim=1) #shape=[B, F + F_pitch, T] = shape=[B, 2F, T]
        ##############

        #self.df = self.df_layer(stft)

        # print(f"nan: {spectrum.isnan().any()}")
        # print(f"inf: {spectrum.isinf().any()}")
        if self.middle_vad:
            if self.middle_layer:
                masks_b, output_vad_sum, out_mean_p_list = self.TCN(self.spectrum, vas_sum_label)
                B, f, T = masks_b.shape
                masks_b = torch.reshape(masks_b, (B, 2, -1, T))
                out_mean_p_list.append(masks_b)
                #print(len(out_mean_p_list))
                out_mean_p_all = torch.stack(out_mean_p_list, dim=1) #B, 4, 2, F, T]
                self.masks_b = torch.stack([self.conv2d_middle(out_mean_p_all[:,:,spk]) for spk in range(self.num_spk)], dim=1) #B, 2, F, T]
                #self.masks_b = F.pad(torch.mean(torch.stack(out_mean_p_list, dim=1), dim=1), (0,0,2,0), value=0)
                self.masks_b =  F.pad(self.masks_b, (0,0,1,0), value=0)
                self.masks_b = torch.reshape(self.masks_b, (B, -1, T))
                #self.masks_b =  F.pad(self.masks_b, (0,0,2,0), value=0)
            else:
                self.masks_b, output_vad_sum = self.TCN(self.spectrum, vas_sum_label)
            #self.masks_b.register_hook(self.activations_hook) #for test.py
            output_vad_sum = torch.squeeze(output_vad_sum)
        else:
            if self.middle_layer:
                masks_b, out_mean_p_list = self.TCN(self.spectrum)
                B, f, T = masks_b.shape
                masks_b = torch.reshape(masks_b, (B, 2, -1, T))
                out_mean_p_list.append(masks_b)
                #print(len(out_mean_p_list))
                out_mean_p_all = torch.stack(out_mean_p_list, dim=1) #B, 4, 2, F, T]
                self.masks_b = torch.stack([self.conv2d_middle(out_mean_p_all[:,:,spk]) for spk in range(self.num_spk)], dim=1) #B, 2, F, T]
                #self.masks_b = F.pad(torch.mean(torch.stack(out_mean_p_list, dim=1), dim=1), (0,0,2,0), value=0)
                self.masks_b =  F.pad(self.masks_b, (0,0,1,0), value=0)
                self.masks_b = torch.reshape(self.masks_b, (B, -1, T))
                #out_mean_p_list.append(torch.unsqueeze(self.conv2d_middle(torch.unsqueeze(masks_b, dim=1), dim=1)))
                #self.masks_b = F.pad(torch.mean(torch.stack(out_mean_p_list, dim=1), dim=1), (0,0,2,0), value=0)
                #self.masks_b = F.pad(torch.sum(torch.stack(out_mean_p_list, dim=1), dim=1), (0,0,2,0), value=0)
            else:
                #self.masks_b = self.TCN(self.spectrum)
                self.masks_b = self.TCN(self.spectrum)
            output_vad_sum = 0
            
            #self.masks_b.register_hook(self.activations_hook) #for test.py
            #self.masks = self.TCN(df) #ToDO df
        #self.masks = self.m(self.masks * self.slope) ############THIS
        #self.masks = self.m(self.masks_b)
        self.masks = self.masks_b
        if self.final_vad:
            batch_size, freq_mul_speakers_size, frames_number = self.masks_b.shape #need to change for regular vad
            self.mask_per_speaker_vad = self.masks_b.reshape(batch_size, self.num_spk, self.n_fftBins_h, frames_number) #need to change for regular vad
            output_vad = torch.cat([self.vad(self.mask_per_speaker_vad[:, s]) for s in range(self.num_spk)], dim=1) #self.vad(self.masks_b) need to change for regular vad
        else:
            output_vad = 0
        
        # self.masks = self.to_spec(x) #shape = [B, self.n_fftBins_h*self.num_spk, frames_number]
        if self.complex_masks:
            batch_size, freq_mul_speakers_size_mul2, frames_number = self.masks.shape
            self.mask_per_speaker = self.masks.reshape(batch_size, self.num_spk, 2, self.n_fftBins_h, frames_number) #2 it is for real and imaginary parts
            #print(self.mask_per_speaker.dtype)
            comlx_masks = torch.complex(real=self.mask_per_speaker[:, :, 0], imag=self.mask_per_speaker[:, :, 1])
            #print(comlx_masks.shape)
            self.estimated_stfts = torch.mul(torch.unsqueeze(stft_out[:,0,:,:], 1), comlx_masks)
        else:
            batch_size, freq_mul_speakers_size, frames_number = self.masks.shape
            self.mask_per_speaker = self.masks.reshape(batch_size, self.num_spk, self.n_fftBins_h, frames_number)
            ##attention between spekaers

            """self.mask_activity = torch.stack([torch.squeeze(self.mask_conv2d(torch.unsqueeze(self.mask_per_speaker[:, s], dim=1)), dim=1) for s in range(self.num_spk)], dim=1)
            self.mask_activity = self.prelu_mask(self.mask_activity) ###add prelu
            self.mask_per_speaker = self.mask_per_speaker * torch.flip(self.mask_activity, dims=[1])"""
            
            ##attention between spekaers
            
            #for activity in output
            """self.mask_activity = torch.stack([torch.squeeze(self.activity_output(torch.unsqueeze(self.mask_per_speaker[:, s], dim=1)), dim=1) for s in range(self.num_spk)], dim=1)
            self.mask_per_speaker = self.mask_per_speaker * self.mask_activity"""
            self.mask_per_speaker = self.m(self.mask_per_speaker)
            
            #self.mask_per_speaker[self.mask_per_speaker < 0.3] = 0
            #for activity in output
            if self.noisy_phase:
                
                stft_mags = stft_out[:,0,:,:].abs()
                noisy_stft_phase = stft_out[:,0,:,:].angle()
                masked_mag = torch.mul(torch.unsqueeze(stft_mags, 1), self.mask_per_speaker)
                self.estimated_stfts = masked_mag.type(torch.complex64) * torch.unsqueeze(torch.exp(1j*noisy_stft_phase), 1)
                
                #print(self.estimated_stfts.imag)
                #print(self.estimated_stfts.real)
                ##Correct Phase
                if self.pc_bool:
                    self.estimated_stfts_pc = []
                    for spk in range(self.num_spk):
                        #print(self.estimated_stfts[:, spk].shape)
                        temp_abs = self.abs_layer(torch.unsqueeze(self.estimated_stfts[:, spk].abs(), dim=1)) * torch.unsqueeze(self.estimated_stfts[:, spk].abs(), dim=1)
                        temp_abs = torch.squeeze(temp_abs, dim=1)
                        #new_pahse = self.PI * self.tanh_phase(torch.squeeze(self.correct_phase_layer(torch.stack((self.estimated_stfts[:, spk].abs(), noisy_stft_phase), dim=1)), dim=1))
                        new_pahse = self.PI * self.tanh_phase(noisy_stft_phase - torch.squeeze(self.correct_phase_layer(torch.stack((temp_abs, noisy_stft_phase), dim=1)), dim=1))
                        #print(new_pahse.shape)
                        self.estimated_stfts_pc.append(self.estimated_stfts[:, spk].abs() * torch.exp(1j*new_pahse))
                    self.estimated_stfts = torch.stack(self.estimated_stfts_pc, dim=1)                             
                    ##Correct Phase
                ########################### Only in inference
                """if self.final_vad:
                    #print(vas_sum_label.shape)
                    output_vad_temp = torch.where(torch.mean(output_vad, dim=1, keepdim=True) >= 0.5, 1, 0)
                    self.estimated_stfts = torch.mul(torch.unsqueeze(output_vad_temp, 1), self.estimated_stfts)"""
                ########################### Only in inference
                
            else:    
                ########################### Only in inference
                """if self.final_vad:
                    #print(vas_sum_label.shape)
                    output_vad_temp = torch.where(torch.mean(output_vad, dim=1, keepdim=True) >= 0.5, 1, 0)
                    self.estimated_stfts = torch.mul(torch.unsqueeze(stft_out[:,0,:,:] * output_vad_temp, 1), self.mask_per_speaker)"""
                ########################### Only in inference
                self.estimated_stfts = torch.mul(torch.unsqueeze(stft_out[:,0,:,:], 1), self.mask_per_speaker) #shape = [B, 2, F, T]
                
        #apply each mask on stft,
        # the unsqueeze action is intended to  convert
        # the stft's shape to [B, 1, self.n_fftBins_h, frames_number]# for broadcasting
        ##########
        #use estimated stft instead mask as input to the VAD model
        #power = torch.pow(torch.abs(self.estimated_stfts), 2) #for multi channel,
        # take only the channel zero, shape=[B, F, T]
        #spectrum = self.am_to_db(power) 
        #output_vad = self.vad(torch.reshape(spectrum, (self.masks.shape)))
        ###########
        
        ########################### Only in inference
        """if self.final_vad:
            self.estimated_stfts = torch.where(torch.unsqueeze(output_vad, dim=2) >=0.5, 1, 0) * self.estimated_stfts"""
        ########################### Only in inference

        out_separation = self.inv_spec(self.estimated_stfts, length=num_of_samples)  # shape = [B, self.num_spk, time]                                    

        #out_separation = self.inv_spec(self.estimated_stfts_pc, length=num_of_samples)  # shape = [B, self.num_spk, time]
        
        ##Normalizing
        #out_separation = 2*(out_separation - torch.min(out_separation, dim=-1, keepdim=True)[0]) / (torch.max(out_separation, dim=-1, keepdim=True)[0] - torch.min(out_separation, dim=-1, keepdim=True)[0]) - 1
        
        #print(f"the out shape is: {out_separation.shape}")
        # print('self.estimated_stfts.shape', self.estimated_stfts.shape)
        
        if self.csd_bool:
            out_csd = [self.csd_model(self.estimated_stfts[:,spk,:,:]) for spk in range(self.num_spk)]
            output_csd = torch.stack(out_csd, dim=-2) #shape=[B, num_class, num_spk, num_frames]=[B, 3, 2, 625]

            sum_estimated_stft = torch.sum(self.estimated_stfts, dim=1) #sum over the spk dim, shape
            # =[batch_size, self.n_fftBins_h, frames_number]
            out_csd_sum = self.csd_model(sum_estimated_stft) #if you want csd so abort this comment
        else:
            output_csd = 0
            out_csd_sum = 0
        # print(self.estimated_stfts.shape)
        # input_csd_temp = torch.mul(torch.unsqueeze(spectrum, 1), self.mask_per_speaker) #shape = [B, num_spk, F, T]
        # input_csd = input_csd_temp.reshape(self.masks.shape)  #shape = [B, num_spk * F, T]
        # csd_output = self.csd(input_csd)
        
            
        return out_separation, output_vad, output_csd, out_csd_sum, self.estimated_stfts, output_vad_sum


    # hook for the gradients of the activations
    def activations_hook(self, grad):
        self.gradients = grad
     # method for the gradient extraction
    def get_activations_gradient(self):
        return self.gradients
    

if __name__ == '__main__':
    s = SeparationModel()
    # y= np.random.random(size=(10, 257, 512)).astype(np.double) # + j*np.random.random(size=(1, 257, 512))
    x = torch.rand(size=(10, 6, 48000))
    doa = 360 * torch.rand(size=(10, 1, 2))  # in deg
    # y = torch.from_numpy(y)
    out = s(x, doa)
    print("done")
    