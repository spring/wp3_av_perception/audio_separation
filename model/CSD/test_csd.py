
import torch.nn as nn

# from STFT_Using_Conv1.STFT_Conv import STFT
# from base import BaseModel
import torch
import numpy as np

from torchaudio import transforms
from collections import OrderedDict
from torch.utils.data import Dataset
import pandas as pd
import torch ##Asasasasa
import pickle
import numpy as np
from scipy.io.wavfile import write
import os 
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SubsetRandomSampler
from torch.utils.data.dataloader import default_collate
import argparse
import json
import logging
from pathlib import Path
import wandb
import sys

from datetime import datetime
import matplotlib.pyplot as plt

SEED = 123
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)

class CSD(nn.Module):
    def __init__(self, n_fftBins):
        super().__init__()
        self.n_fftBins = n_fftBins
        self.n_fftBins_h = n_fftBins//2 + 1
        self.spec = transforms.Spectrogram(n_fft=self.n_fftBins, hop_length=256, win_length=self.n_fftBins,
                                      window_fn=torch.hann_window, power=None)  # for all channels
        self.am_to_db = transforms.AmplitudeToDB(stype="power")
        
        padding = 5//2
        self.block1 = nn.Sequential(OrderedDict([
          ('conv1_1', nn.Conv1d(in_channels=self.n_fftBins_h, out_channels=self.n_fftBins_h, kernel_size=5, stride=1, dilation=2 ** 0, padding=2 ** 0 + (5//2 - 1))),
          ('relu_1', nn.ReLU()),
          ('BN_1', nn.BatchNorm1d(self.n_fftBins_h))
        ]))

        self.block2 = nn.Sequential(OrderedDict([
          ('conv1_2', nn.Conv1d(in_channels=self.n_fftBins_h, out_channels=128, kernel_size=3, stride=1, dilation=2 ** 1, padding=2 ** 1 + (3//2 - 1))),
          ('relu_2', nn.ReLU()),
          ('BN_2', nn.BatchNorm1d(128))
        ]))
        
        self.block3 = nn.Sequential(OrderedDict([
          ('conv1_3', nn.Conv1d(in_channels=128, out_channels=32, kernel_size=3, stride=1, dilation=2 ** 2, padding=2 ** 2 + (3//2 - 1))),
          ('relu_3', nn.ReLU()),
          ('BN_3', nn.BatchNorm1d(32))
        ]))

        self.block4 = nn.Sequential(OrderedDict([
          ('conv1_4', nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, dilation=2 ** 3, padding=2 ** 3 + (3//2 - 1))),
          ('relu_4', nn.ReLU()),
          ('BN_4', nn.BatchNorm1d(16))
        ]))

        self.output_layer = nn.Conv1d(in_channels=16, out_channels=3, kernel_size=3, dilation=2 ** 4, padding=2 ** 4 + (3//2 - 1))
        

    def forward(self, input):
        #input shape is [B, samples in time]
        stft = self.spec(input)
        stft[:, 0, :] = 0
        #print(f"the stft shape is: {stft.shape}")
        power = torch.pow(torch.abs(stft), 2) #shape=[B, F, T]
        spectrum = self.am_to_db(power)
        out = self.block1(spectrum)
        out = self.block2(out)
        out = self.block3(out)
        out = self.block4(out)
        spp = self.output_layer(out)
        return spp

class NoisyWsjDataSet(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, csv_file, cds_lables, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.cds_lables = cds_lables
        self.recording_df = pd.read_csv(csv_file)
        self.transform = transform

    def __len__(self):
        #a = 100
        #return a
        return self.recording_df.shape[0]



    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        record_path = self.recording_df.loc[idx, "path_file"]
        with open(record_path, "rb") as f:
            mixed_sig_np, _, _ = pickle.load(f)
            

        path_label_idx = os.path.join(self.cds_lables, "scenario_{0}.npz".format(idx))
        reverb = self.recording_df.loc[idx, "rt60"]
        label = np.load(path_label_idx)
        
        if self.transform:
            mixed_sig_np = self.transform(mixed_sig_np)


        sample_separation = {'mixed_signals': mixed_sig_np[1], "reverb":reverb}
        label_csd = {"vad_frames_sum": label["vad_frames_sum"]}
        # mixed_signals.shape = [num of channels, num of sampples got this particular audio]
        # clean_speeches.shape = [num of speakers, num of sampples got this particular audio]
        # doa.shape = number of speakers, the doa to the center of the glass
        return sample_separation, label_csd   

class BaseDataLoader(DataLoader):
    """
    Base class for all data loaders
    """
    def __init__(self, dataset, batch_size, shuffle, validation_split, num_workers, pin_memory, collate_fn=default_collate):
        self.validation_split = validation_split
        self.shuffle = shuffle

        self.batch_idx = 0
        self.n_samples = len(dataset)

        self.sampler, self.valid_sampler = self._split_sampler(self.validation_split)

        self.init_kwargs = {
            'dataset': dataset,
            'batch_size': batch_size,
            'shuffle': self.shuffle,
            'collate_fn': collate_fn,
            'num_workers': num_workers,
            'pin_memory': pin_memory
        }
        super().__init__(sampler=self.sampler, **self.init_kwargs)

    def _split_sampler(self, split):
        if split == 0.0:
            return None, None

        idx_full = np.arange(self.n_samples)

        np.random.seed(0)
        np.random.shuffle(idx_full)

        if isinstance(split, int):
            assert split > 0
            assert split < self.n_samples, "validation set size is configured to be larger than entire dataset."
            len_valid = split
        else:
            len_valid = int(self.n_samples * split)

        valid_idx = idx_full[0:len_valid]
        train_idx = np.delete(idx_full, np.arange(0, len_valid))

        train_sampler = SubsetRandomSampler(train_idx)
        valid_sampler = SubsetRandomSampler(valid_idx)

        # turn off shuffle option which is mutually exclusive with sampler
        self.shuffle = False
        self.n_samples = len(train_idx)

        return train_sampler, valid_sampler

    def split_validation(self):
        if self.valid_sampler is None:
            return None
        else:
            return DataLoader(sampler=self.valid_sampler, **self.init_kwargs)             


class NoisyWsjDataLoader(BaseDataLoader):
    """
    MNIST data loading demo using BaseDataLoader
    """
    def __init__(self, csv_file, cds_lables, batch_size, shuffle=True, validation_split=0.0, num_workers=1, pin_memory=False):
        self.csv_file = csv_file
        self.dataset = NoisyWsjDataSet(csv_file, cds_lables)
        super().__init__(self.dataset, batch_size, shuffle, validation_split, num_workers, pin_memory)


def prepare_device(n_gpu_use):
    """
    setup GPU device if available. get gpu device indices which are used for DataParallel
    """
    n_gpu = torch.cuda.device_count()
    print(f"There is {n_gpu} gpu available")
    if n_gpu_use > 0 and n_gpu == 0:
        print("Warning: There\'s no GPU available on this machine,"
              "training will be performed on CPU.")
        n_gpu_use = 0
    if n_gpu_use > n_gpu:
        print(f"Warning: The number of GPU\'s configured to use is {n_gpu_use}, but only {n_gpu} are "
              "available on this machine.")
        n_gpu_use = n_gpu
    device = torch.device('cuda:0' if n_gpu_use > 0 else 'cpu')
    list_ids = list(range(n_gpu_use))
    return device, list_ids

def _progress(batch_idx):
        base = '[{}/{} ({:.0f}%)]'
        if hasattr(data_loader, 'n_samples'):
            current = batch_idx * data_loader.batch_size
            total = data_loader.n_samples
        else:
            print("check!!!")
        return base.format(current, total, 100.0 * current / total)

def accuracy(preds, targets):
    """
    preds: with shape=[B, num_class, T]
    targets: with shape=[B, T]
    """
    batch_size = targets.shape[0]
    preds_class = torch.argmax(preds, dim=1)
    acc = torch.sum(preds_class==targets) / batch_size
    return acc

def save_csd(csd_output, target_csd, save_path):
    Path(save_path).mkdir(parents=True, exist_ok=True)
    preds_class = torch.argmax(csd_output.cpu(), dim=1)
    target_csd = target_csd.cpu()
    plt.plot(torch.squeeze(target_csd))
    plt.savefig(f"{save_path}/True_Csd.png")
    plt.close()
    plt.plot(torch.squeeze(preds_class))
    plt.savefig(f"{save_path}/Estimated_Csd.png")
    plt.close()    



args = argparse.ArgumentParser(description='CSD model')
args.add_argument('-c', '--config', default="/home/dsi/moradim/Audio-Visual-separation-using-RTF/model/config_csd.json", type=str, help='config file path (default: None)')
args.add_argument('-r', '--resume', default="/home/dsi/moradim/Results/train_only_csd_regular/Weights/model_best_2022_03_15-09:58:39_PM.pth", type=str, help='config file path (default: None)')
args = args.parse_args()
config_path = args.config
with open(config_path, 'r') as f:
  config = json.load(f)
date = datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p")  
Path(config["tester"]["save_test"] + 'Logs_Test/').mkdir(parents=True, exist_ok=True)

logger = logging.getLogger('CSD-only')
# set log level
logger.setLevel(logging.INFO)
# define file handler and set formatter
file_handler = logging.FileHandler(config["tester"]["save_test"] + 'Logs_Test/' + '/logfile_{}.log'.format(date), mode='w')  # mode='w'
formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(name)s : %(message)s')
file_handler.setFormatter(formatter)
# add file handler to logger
logger.addHandler(file_handler)
consoleHandler = logging.StreamHandler(sys.stdout)
# consoleHandler.setLevel('INFO')
consoleHandler.setFormatter(formatter)
logger.addHandler(consoleHandler)


data_loader = NoisyWsjDataLoader(config['tester']['csv_file_test'],
        config['tester']['cds_lables'],
        batch_size=1,
        shuffle=False,
        validation_split=0.0,
        num_workers=0)

criterion = nn.CrossEntropyLoss(reduction='sum')
model = CSD(n_fftBins=config["arch"]["args"]["n_fftBins"])
device, device_ids = prepare_device(config['n_gpu'])
model = model.to(device)
if len(device_ids) > 1:
    model = torch.nn.DataParallel(model, device_ids=device_ids)
#print(type(trainable_params))
trainable_params = filter(lambda p: p.requires_grad, model.parameters())
optimizer = getattr(torch.optim, config["optimizer"]["type"])(**config["optimizer"]["args"], params=trainable_params) 
if args.resume is not None:
    """
    Resume from saved checkpoints

    :param resume_path: Checkpoint path to be resumed
    """
    resume_path = args.resume
    resume_path = str(resume_path)
    logger.info("Loading checkpoint: {} ...".format(resume_path))
    checkpoint = torch.load(resume_path)
    start_epoch = checkpoint['epoch'] + 1

    # load architecture params from checkpoint.
    if checkpoint['config']['arch'] != config['arch']:
        logger.warning("Warning: Architecture configuration given in config file is different from that of "
                            "checkpoint. This may yield an exception while state_dict is being loaded.")
    model.load_state_dict(checkpoint['state_dict'])

    # load optimizer state from checkpoint only when optimizer type is not changed.
    if checkpoint['config']['optimizer']['type'] != config['optimizer']['type']:
        logger.warning("Warning: Optimizer type given in config file is different from that of checkpoint. "
                            "Optimizer parameters not being resumed.")
    else:
        optimizer.load_state_dict(checkpoint['optimizer'])

    logger.info("Checkpoint loaded. Resume training from epoch {}".format(start_epoch))



  
num_of_param = sum(p.numel() for p in model.parameters() if p.requires_grad)
print("Number of parameters that require grad in the model is: {num}".format(num=num_of_param)) 

'''
------------------------------------
val loop'''
model.eval()
total_loss = 0.0
totatl_acc = 0.0
Path(config["tester"]["save_test"] + 'CSD_result_png/').mkdir(parents=True, exist_ok=True)
with torch.no_grad():
    for batch_idx, (sample_separation, label_csd)  in enumerate(data_loader):
        mixed_signal = sample_separation['mixed_signals']
        mixed_signal, label_csd["vad_frames_sum"] = mixed_signal.to(device), label_csd["vad_frames_sum"].to(device).to(torch.long)
        #print(f"target shape is: {target.shape}")
        
        csd_output = model(mixed_signal)
        save_path = config["tester"]["save_test"] + 'CSD_result_png/' + 'sample_{}'.format(batch_idx)
        Path(save_path).mkdir(parents=True, exist_ok=True)
        save_csd(csd_output, label_csd["vad_frames_sum"], save_path)
        #reduce_kwargs = {'src': target} #I dont do reduce with csd
        loss = criterion(csd_output, label_csd["vad_frames_sum"])
        loss = loss / mixed_signal.shape[0] #divide in batch size
        total_loss += loss.detach()
        acc = accuracy(csd_output, label_csd["vad_frames_sum"]).detach()
        logger.info(f"Batch accuracy = {acc}")
        totatl_acc += acc
    epoch_loss = total_loss/(batch_idx + 1)
    epoch_acc = totatl_acc/(batch_idx + 1)
    logger.info(f"Epoch Test accuracy: {epoch_acc}")
    logger.info(f"Epoch Test loss: {epoch_loss}")
    
            

                   